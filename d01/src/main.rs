use std::io::BufRead;
fn main() {
    let input: Vec<String> = std::io::stdin()
        .lock()
        .lines()
        .map(|l| l.expect("Error reading line :("))
        .collect();

    let output = run(input);
    println!("{}", output[0]);

    let sum: u32 = output[..3].iter().sum();
    println!("{sum}");
}

fn run(input: Vec<String>) -> Vec<u32> {
    let mut lists: Vec<Vec<u32>> = vec![vec![]];
    input.iter().for_each(|line| {
        if let Ok(n) = line.trim().parse::<u32>() {
            let len = lists.len();
            lists[len - 1].push(n);
        } else {
            lists.push(vec![]);
        }
    });

    let mut output: Vec<u32> = lists.iter().map(|l| l.iter().sum()).collect();

    output.sort();
    output.reverse();
    output
}

#[cfg(test)]
mod d01 {
    use crate::*;
    #[test]
    fn example() {
        let output = include_str!("../example.out").trim();
        let input = include_str!("../example.in")
            .lines()
            .map(|l| l.to_owned())
            .collect();
        let r_output = run(input)[0].to_string();

        assert_eq!(output, r_output)
    }
}
