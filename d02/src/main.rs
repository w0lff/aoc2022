use std::io::BufRead;
fn main() {
    let input: Vec<String> = std::io::stdin()
        .lock()
        .lines()
        .map(|l| l.expect("Error reading line :("))
        .collect();

    let output = run(input);
    println!("{}", output[0]);
    /*
        let sum: u32 = output[..3].iter().sum();
        println!("{sum}");
    */
}

fn run(input: Vec<String>) -> Vec<u32> {
    let mut outcomes: Vec<u32> = vec![];
    input.iter().for_each(|line| {
        let line: Vec<&str> = line.trim().split(' ').collect();
        let opponent = match_input_to_rps(line[0]).unwrap();
        let player = match_input_to_rps(line[1]).unwrap();

        let score = score(game(opponent, &player), player);
        outcomes.push(score)
    });

    // let mut output: Vec<u32> = lists.iter().map(|l| l.iter().sum()).collect();
    let output: u32 = outcomes.iter().sum();

    vec![output]
}

fn game(opponent: Shape, player: &Shape) -> Outcome {
    match (opponent, player) {
        (Shape::Rock, Shape::Rock) => Outcome::Draw,
        (Shape::Rock, Shape::Paper) => Outcome::Win,
        (Shape::Rock, Shape::Scissors) => Outcome::Loose,
        (Shape::Paper, Shape::Rock) => Outcome::Loose,
        (Shape::Paper, Shape::Paper) => Outcome::Draw,
        (Shape::Paper, Shape::Scissors) => Outcome::Win,
        (Shape::Scissors, Shape::Rock) => Outcome::Win,
        (Shape::Scissors, Shape::Paper) => Outcome::Loose,
        (Shape::Scissors, Shape::Scissors) => Outcome::Draw,
    }
}
fn score(outcome: Outcome, player: Shape) -> u32 {
    outcome as u32 + player as u32
}

fn match_input_to_rps(input: &str) -> Option<Shape> {
    match input {
        "A" | "X" => Some(Shape::Rock),
        "B" | "Y" => Some(Shape::Paper),
        "C" | "Z" => Some(Shape::Scissors),
        _ => None,
    }
}

#[derive(Copy, Clone)]
enum Outcome {
    Loose = 0,
    Draw = 3,
    Win = 6,
}

#[derive(Copy, Clone)]
enum Shape {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

#[cfg(test)]
mod d01 {
    use crate::*;
    #[test]
    fn example() {
        let output = include_str!("../example.out").trim();
        let input = include_str!("../example.in")
            .lines()
            .map(|l| l.to_owned())
            .collect();
        let r_output = run(input)[0].to_string();

        assert_eq!(output, r_output)
    }
}
